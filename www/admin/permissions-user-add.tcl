ad_page_contract {
    Redirect page for adding users to the permissions list.
    
    @author Lars Pind (lars@collaboraid.biz)
    @creation-date 2003-06-13
    @cvs-id $Id: permissions-user-add.tcl,v 1.1 2003/09/22 15:45:11 lars Exp $
}

set object_id [ad_conn package_id]

set page_title "Add User"

set context [list [list "permissions" "Permissions"] $page_title]

